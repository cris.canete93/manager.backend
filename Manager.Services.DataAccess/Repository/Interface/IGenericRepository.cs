﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Services.DataAccess.Repository.Interface
{
    /// <summary>
    /// Contract with repository responsibility
    /// </summary>
    /// <typeparam name="T">Entity generic</typeparam>
    public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// Method in charge of obtaining the total of the records
        /// </summary>
        /// <returns></returns>
        Task<int> GetCountRecordAsync();

        /// <summary>
        /// Get single entity by int primaryKey
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        Task<T> FindElementAsync(int primaryKey);

        /// <summary>
        /// Method in charge of obtaining an element
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<List<T>> GetAllListAsync(int page, int qtyRecords, Func<IQueryable<T>, IQueryable<T>> includeMembers);

        /// <summary>
        ///Method responsible for obtaining a list of elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<T> GetByElementAsync(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// Method in charge of validating if the entity exists.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        Task<bool> ExistsRecordAsync(Expression<Func<T, bool>> predicate);
    }
}
