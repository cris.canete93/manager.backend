﻿using Manager.Services.DataAccess.Repository.Interface;
using Manager.Services.DataAccess.UnitOfWork.Interface;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Manager.Services.DataAccess.Repository.Implementation
{
    /// <summary>
    /// Class the implement contract with repository responsibility
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        #region Properties IoC
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Cosntructor
        /// <summary>
        /// Repository constructor which will inject the database context
        /// </summary>
        public GenericRepository(IUnitOfWork unitOfWork)
            => _unitOfWork = unitOfWork;

        #endregion

        #region Public Methods

        /// <summary>
        /// Method in charge of obtaining the total of the records
        /// </summary>
        /// <returns></returns>
        public async Task<int> GetCountRecordAsync()
            => await _unitOfWork.DBContext.Set<T>().CountAsync();

        /// <summary>
        /// Get single entity by int primaryKey
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public async Task<T> FindElementAsync(int primaryKey)
           => await _unitOfWork.DBContext.Set<T>().FindAsync(primaryKey);

        /// <summary>
        /// Method in charge of obtaining an element
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<List<T>> GetAllListAsync(int page, int qtyRecords, Func<IQueryable<T>, IQueryable<T>> includeMembers)
        {
            DbSet<T> set = _unitOfWork.DBContext.Set<T>();
            IQueryable<T> result = includeMembers(set);
            return await result.Skip((page - 1) * qtyRecords)
                         .Take(qtyRecords)
                          .ToListAsync();
        }
        /// <summary>
        ///Method responsible for obtaining a list of elements
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<T> GetByElementAsync(Expression<Func<T, bool>> predicate)
            => await _unitOfWork.DBContext.Set<T>().Where(predicate).FirstOrDefaultAsync();

        /// <summary>
        /// Method in charge of validating if the entity exists.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<bool> ExistsRecordAsync(Expression<Func<T, bool>> predicate)
            => await _unitOfWork.DBContext.Set<T>().AnyAsync(predicate);
        #endregion


    }
}
