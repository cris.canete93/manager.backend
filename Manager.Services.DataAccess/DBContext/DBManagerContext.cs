﻿using Manager.Services.Entities.Model.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Manager.Services.DataAccess.DBContext
{
    /// <summary>
    /// Class the model database 
    /// </summary>
    public class DBManagerContext : DbContext
    {
        #region Constructors
        /// <summary>
        /// Constructor responsible for starting the DB context
        /// </summary>
        /// <param name="options"></param>
        public DBManagerContext(DbContextOptions options) : base(options) 
        {   }
        #endregion

        #region Properties
        public DbSet<Auth> Auth { get; set; }
        public DbSet<Users> Users { get; set; }
        #endregion
    }
}
