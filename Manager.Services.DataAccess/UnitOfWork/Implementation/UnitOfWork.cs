﻿using Manager.Services.DataAccess.DBContext;
using Manager.Services.DataAccess.UnitOfWork.Interface;
using Microsoft.Data.SqlClient;
using Microsoft.Data.Sql;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Manager.Services.DataAccess.UnitOfWork.Implementation
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {

        #region Properties Ioc
        public DBManagerContext DBContext { get; set; }
        #endregion

        #region Constuctor
        /// <summary>
        /// Repository constructor which will inject the database context
        /// </summary>
        /// </summary>
        /// <param name="_currentContext"></param>
        public UnitOfWork(DBManagerContext _currentContext)
        {
            DBContext = _currentContext;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method in charge of executing persistence
        /// </summary>
        /// <returns></returns>
        public async Task<bool> CommitAsync()
        {
            bool isSuccesFully = false;
            try
            {
                using (var Transaccion = DBContext.Database.BeginTransaction())
                {

                    var result = await DBContext.SaveChangesAsync();

                    if (result > 0)
                    {
                        Transaccion.Commit();
                        isSuccesFully = true;
                    }
                    else
                        Transaccion.Rollback();
                }
            }
            finally
            {
                Dispose();
            }

            return await Task.FromResult(isSuccesFully);
        }

        /// <summary>
        /// Method in charge of the process of saving xml information
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="parm"></param>
        /// <returns></returns>
        public async Task<int> ExecWithStoreProcedure(string spName, SqlParameter[] parameters)
            => await DBContext.Database.ExecuteSqlRawAsync(spName, parameters);
        
        #endregion

        #region Public Methods
        /// <summary>
        /// Clean data chache
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
