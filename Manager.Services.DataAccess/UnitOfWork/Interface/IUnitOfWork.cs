﻿using Manager.Services.DataAccess.DBContext;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Services.DataAccess.UnitOfWork.Interface
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Property the DBContext 
        /// </summary>
        DBManagerContext DBContext { get; }

        /// <summary>
        /// Method in charge of executing the saving of the data
        /// </summary>
        /// <returns></returns>
        Task<bool> CommitAsync();

        /// <summary>
        /// Method in charge of the process of saving xml information
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="parm"></param>
        /// <returns></returns>
        Task<int> ExecWithStoreProcedure(string spName, SqlParameter[] parameters);
    }
}
