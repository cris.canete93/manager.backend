﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manager.Services.Entities.DTO.GeneralDTO
{
    /// <summary>
    /// Data transfer object
    /// </summary>
    public class CredentialDTO
    {
        public string Parameters { get; set; }
        public string Password { get; set; }
    }
}
