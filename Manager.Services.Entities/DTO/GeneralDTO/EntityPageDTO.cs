﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manager.Services.Entities.DTO.GeneralDTO
{
    /// <summary>
    /// data object transfer
    /// </summary>
    /// <typeparam name="T">Tipo generico</typeparam>
    public class EntityPageDTO<T>
    {
        #region Properties
        public int TotalPages { get; set; }
        public T Records { get; set; }
        public int CurrentPage { get; set; }
        public int TotalRecords { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor para la creacion del paginado
        /// </summary>
        public EntityPageDTO() { }

        /// <summary>
        /// Constructor para la creacion del paginado
        /// </summary>
        /// <param name="totalPage">Total de paginas</param>
        /// <param name="value">Valor generico</param>
        /// <param name="currentPage">Pagina actual</param>
        public EntityPageDTO(int totalPage, T value, int currentPage, int totalRecords)
        {
            TotalPages = totalPage;
            Records = value;
            CurrentPage = currentPage;
            TotalRecords = totalRecords;
        }
        #endregion

    }
}
