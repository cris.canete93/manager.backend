﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manager.Services.Entities.DTO.UserDTO
{
    /// <summary>
    /// Data transfer object
    /// </summary>
    public class UserDTO
    {
        public int? UserId { get; set; }
        public string Email { get; set; }
        public string Sex { get; set; }
        public int? AuthId { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string TypeOperation { get; set; }
        public string Token { get; set; }
        public bool Status { get; set; }
    }
}
