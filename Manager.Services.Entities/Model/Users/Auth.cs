﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Manager.Services.Entities.Model.Users
{
    /// <summary>
    /// Model entity
    /// </summary>
    public class Auth
    {
        [Key]
        public int AuthId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
