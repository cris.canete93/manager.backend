﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Manager.Services.Entities.Model.Users
{
    /// <summary>
    /// Model entity
    /// </summary>
    public class Users
    {
        [Key]
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Sex { get; set; }

        [ForeignKey(nameof(Auth))]
        public int AuthId { get; set; }
       
        public bool Active { get; set; }
        public virtual Auth Auth { get; set; }
    }
}
