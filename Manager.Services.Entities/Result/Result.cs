﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Manager.Services.Entities.Result
{
    /// <summary>
    /// The class is responsible for returning a response.
    /// </summary>
    /// <typeparam name="T">Valor Generico</typeparam>
    public class Result<T>
    {
        #region Porperties
        public T Data { get; set; }
        public bool IsSuccesFully { get; set; }
        public Dictionary<string, string> Errors { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor defaul
        /// </summary>
        public Result() { }

        /// <summary>
        /// Constructor to create with an object
        /// </summary>
        /// <param name="value"></param>
        public Result(T value)
        {
            Data = value;
            IsSuccesFully = true;
            Errors = new Dictionary<string, string>();
        }

        /// <summary>
        /// Constructor to create with an object and erros
        /// </summary>
        /// <param name="value">value generic</param>
        /// <param name="errors">Diccionario the errors</param>
        public Result(T value, Dictionary<string, string> errors)
        {
            Data = value;
            Errors = errors;
            IsSuccesFully = errors == null ? true : errors.Count <= 0;
        }
        #endregion

    }
}
