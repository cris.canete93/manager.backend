﻿using Manager.Services.Entities.DTO.UserDTO;
using Manager.Services.Entities.Result;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Services.Business.UserManagementRules.Interface
{
    /// <summary>
    /// Contract with repository ManagementRules
    /// </summary>
    public interface IAuthManagementRules
    {
        /// <summary>
        /// Method in charge of the validation of the user's credit
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<Result<UserDTO>> AuthValidUserCrendecial(string parameter, string password);
    }
}
