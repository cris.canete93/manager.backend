﻿using Manager.Services.Entities.DTO.GeneralDTO;
using Manager.Services.Entities.DTO.UserDTO;
using Manager.Services.Entities.Result;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Services.Business.UserManagementRules.Interface
{
    /// <summary>
    /// Contract with repository ManagementRules
    /// </summary>
    public interface IUserManagementRules
    {
        /// <summary>
        /// Method responsible for the persistence 
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        Task<Result<bool>> SaveCurrentUser(UserDTO userDTO);

        /// <summary>
        /// Method responsible for the get all users 
        /// </summary>
        /// <returns></returns>
        Task<Result<EntityPageDTO<List<UserDTO>>>> GetAllUser(int? page);

        /// <summary>
        /// Method responsible for the desactive user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<Result<bool>> DeleteUser(string userId);

        /// <summary>
        /// Method responsible for the get user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<Result<UserDTO>> GetUserById(int? userId);
    }
}
