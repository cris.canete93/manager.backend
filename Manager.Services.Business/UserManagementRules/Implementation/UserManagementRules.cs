﻿using Manager.Services.Business.Base;
using Manager.Services.Business.UserManagementRules.Interface;
using Manager.Services.DataAccess.Repository.Interface;
using Manager.Services.DataAccess.UnitOfWork.Interface;
using Manager.Services.Entities.DTO.GeneralDTO;
using Manager.Services.Entities.DTO.UserDTO;
using Manager.Services.Entities.Model.Users;
using Manager.Services.Entities.Result;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Manager.Services.Business.UserManagementRules.Implementation
{
    /// <summary>
    /// Class implemt the Contract with repository UserManagementRules
    /// </summary>
    public class UserManagementRules : BaseBusiness, IUserManagementRules
    {
        #region Properties Ioc
        private readonly IGenericRepository<Users> _userRepository;
        private readonly IGenericRepository<Auth> _authRepository;
        private readonly IUnitOfWork _unitOfWork;
        #endregion

        #region Constructor
        /// <summary>
        /// Rules constructor which will inject the respository
        /// </summary>
        /// <param name="userRepository"></param>
        public UserManagementRules( IGenericRepository<Users> userRepository,
                                    IGenericRepository<Auth> authRepository,
                                   IUnitOfWork unitOfWork)
        {
            _authRepository = authRepository;
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method responsible for the persistence 
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public async Task<Result<bool>> SaveCurrentUser(UserDTO userDTO)
        {
            var error = GetErrorDictionaryInstance();
            try
            {
                var validParmeters = ValidParemterUser(userDTO, error);
                if (!validParmeters.IsSuccesFully)
                    return validParmeters;

                var validExist = await ValidUserExist(userDTO, error);
                if (!validExist.IsSuccesFully)
                    return validExist;

                var sqlParamters = new SqlParameter[]
                {
                    new SqlParameter("@authId", userDTO.AuthId ?? 0),
                    new SqlParameter("@userName", userDTO.UserName),
                    new SqlParameter("@password", Encrypt(userDTO.Password)),
                    new SqlParameter("@email", userDTO.Email),
                    new SqlParameter("@sex", userDTO.Sex),
                    new SqlParameter("@typeOperation", userDTO.TypeOperation),
                };

                var result = await _unitOfWork.ExecWithStoreProcedure(@"exec spUserPersistence 
                                    @authId,@userName,@password,@email, @sex, @typeOperation", sqlParamters);

                if (result == 0)
                    error.Add("Error-save-user-01", "¡Ocurrio un error al guardar el usuario!");

                return await Task.FromResult(new Result<bool>(result > 0, error));
            }
            catch (Exception ex)
            {
                error.Add("Exceptio-UserManagementRules", ex.Message);
                return new Result<bool>(false, error);
            }
        }

        /// <summary>
        /// Method responsible for the get all users 
        /// </summary>
        /// <returns></returns>
        public async Task<Result<EntityPageDTO<List<UserDTO>>>> GetAllUser(int? page)
        {
            var error = GetErrorDictionaryInstance();
            try
            {
                int currentPage = page ?? Page;
                decimal totalRecords = await _userRepository.GetCountRecordAsync();
                int totalPage = GetTotalPage(totalRecords);
                var lst = await _userRepository.GetAllListAsync(currentPage, Qty, i => i.Include(inc => inc.Auth));

                var lstPage = lst.Select(s => new UserDTO 
                {
                    UserId = s.UserId,
                    AuthId = s.AuthId,
                    Email = s.Email,
                    Password = s.Auth.Password,
                    UserName = s.Auth.UserName,
                    Sex = s.Sex,
                    Status = s.Active
                }).ToList();

                return await Task.FromResult(new Result<EntityPageDTO<List<UserDTO>>>(
                    new EntityPageDTO<List<UserDTO>>(totalPage,
                                                     lstPage,
                                                     currentPage,
                                                     (int)totalRecords), error));
            }
            catch (Exception ex)
            {
                error.Add("Exceptio-UserManagementRules", ex.Message);
                return new Result<EntityPageDTO<List<UserDTO>>>(new EntityPageDTO<List<UserDTO>>(), error);
            }
        }

        /// <summary>
        /// Method responsible for the desactive user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Result<bool>> DeleteUser(string userId)
        {
            var error = GetErrorDictionaryInstance();
            try
            {
                int result = 0;
                if(string.IsNullOrEmpty(userId))
                    error.Add("Error-DeleteUser-user-01", "¡El id del usuario se encuentra vacío!");

                if (CanDoAction(error))
                {
                    int.TryParse(userId, out int defaultUser);
                    var sqlParamters = new SqlParameter[]
                    {
                        new SqlParameter("@userId",defaultUser)
                    };

                     result = await _unitOfWork.ExecWithStoreProcedure(@"exec spDeactivateUser 
                                         @userId", sqlParamters);

                    if (result == 0)
                        error.Add("Error-DeleteUser-user-01", "¡Ocurrio un error al desactivar el usuario!");
                }

                return await Task.FromResult(new Result<bool>(result > 0, error));
            }
            catch (Exception ex)
            {
                error.Add("Exceptio-UserManagementRules", ex.Message);
                return new Result<bool>(false, error);
            }
        }

        /// <summary>
        /// Method responsible for the get user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<Result<UserDTO>> GetUserById(int? authId)
        {
            var error = GetErrorDictionaryInstance();
            try
            {
                error = ValidIdElment(authId, error);
                if (CanDoAction(error))
                {
                    var result = await _userRepository.GetByElementAsync(w => w.AuthId == authId);
                    return new Result<UserDTO>(new UserDTO 
                    {
                        AuthId = result.AuthId,
                        UserId = result.UserId,
                        UserName = result.Auth.UserName,
                        Email = result.Email,
                        Sex = result.Sex
                    }, error);
                }
                else
                    return new Result<UserDTO>(new UserDTO(), error);
            }
            catch (Exception ex)
            {
                error.Add("Exceptio-UserManagementRules", ex.Message);
                return new Result<UserDTO>(new UserDTO(), error);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Valid parameter users
        /// </summary>
        /// <returns></returns>
        private Result<bool> ValidParemterUser(UserDTO userDTO, Dictionary<string, string> _error)
        {
            if (userDTO == null)
                _error.Add("Error-valid-user-01", "¡Los valores del usuario se encuentran vacios!");
            if (string.IsNullOrEmpty(userDTO.UserName))
                _error.Add("Error-valid-user-02", "¡El usuario se encuentra vacío!");
            if (string.IsNullOrEmpty(userDTO.Password))
                _error.Add("Error-valid-user-03", "¡El password se encuentra vacío!");
            if (string.IsNullOrEmpty(userDTO.Email))
                _error.Add("Error-valid-user-04", "¡El email se encuentra vacío!");
            if (string.IsNullOrEmpty(userDTO.Sex))
                _error.Add("Error-valid-user-05", "¡El tipo sexo encuentra vacío!");

            return new Result<bool>(CanDoAction(_error), _error);
        }

        /// <summary>
        /// Valid Parameter authId
        /// </summary>
        /// <param name="authId"></param>
        /// <param name="_error"></param>
        /// <returns></returns>
        private Dictionary<string, string> ValidIdElment(int? authId, Dictionary<string, string> _error)
        {
            if(!authId.HasValue)
                _error.Add("Error-user-01", "¡El id del usuario se encuentra vacío!");

            return _error;
        }

        /// <summary>
        /// Valid data the user exis
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task<Result<bool>> ValidUserExist(UserDTO user, Dictionary<string, string> _error)
        {
            bool isNotExist = true;
            if (user.TypeOperation == "Insert")
            {
                var isResult = await _authRepository.ExistsRecordAsync(e => e.UserName == user.UserName);
                var isResultEmail = await _userRepository.ExistsRecordAsync(e => e.Email == user.Email);

                if (isResult && isResultEmail)
                {
                    isNotExist = false;
                    _error.Add("Error-user-valid-Exist", "¡Ya existe el siguiente usuario!");
                }
            }

            return new Result<bool>(isNotExist, _error);
        }
        #endregion

    }
}
