﻿using Manager.Services.Business.Base;
using Manager.Services.Business.UserManagementRules.Interface;
using Manager.Services.DataAccess.Repository.Interface;
using Manager.Services.Entities.DTO.UserDTO;
using Manager.Services.Entities.Model.Users;
using Manager.Services.Entities.Result;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Services.Business.UserManagementRules.Implementation
{
    /// <summary>
    /// Class implemt the Contract with repository UserManagementRules
    /// </summary>
    public class AuthManagementRules : BaseBusiness, IAuthManagementRules
    {
        #region PropertiesIoC
        private readonly IGenericRepository<Auth> _authRepository;
        private readonly IGenericRepository<Users> _userRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Rules constructor which will inject the respository
        /// </summary>
        public AuthManagementRules(IGenericRepository<Auth> authRepository,
            IGenericRepository<Users> userRepository)
        {
            _authRepository = authRepository;
            _userRepository = userRepository;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method in charge of the validation of the user's credit
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<Result<UserDTO>> AuthValidUserCrendecial(string parameter, string password)
        {
            var error = GetErrorDictionaryInstance();
            try
            {
                error = ValidParameterLogin(parameter, password, error);
                if (CanDoAction(error))
                {
                    string _password = Encrypt(password);
                    var result = await _authRepository.GetByElementAsync(w => w.Password == _password && w.UserName == parameter);
                    if (result != null)
                    {
                        var resultUser = await _userRepository.GetByElementAsync(w => w.AuthId == result.AuthId && w.Active);
                        if (resultUser != null)
                            return await Task.FromResult(new Result<UserDTO>(new UserDTO 
                            {
                                UserId = resultUser.UserId,
                                AuthId = result.AuthId,
                                Sex = resultUser.Sex,
                                Email = resultUser.Email,
                                UserName = result.UserName
                            }, error));
                        else
                        {
                            error.Add("AuthMsjError", "¡El usuario se encuenra desactivado!.");
                            return await Task.FromResult(new Result<UserDTO>(new UserDTO(), error));
                        }
                    }
                    else
                    {
                        error.Add("AuthMsjError", "El Usuario o la contraseña son erroneas, favor de validar sus datos.");
                        return await Task.FromResult(new Result<UserDTO>(new  UserDTO(), error));
                    }
                }
                else
                    return await Task.FromResult(new Result<UserDTO>(new UserDTO(), error));
            }
            catch (Exception ex)
            {
                error.Add("Exceptio-AuthManagementRules", ex.Message);
                return new Result<UserDTO>(new UserDTO(), error);
            }
        }
        #endregion

        #region Valid Parameter
        /// <summary>
        /// Method in charge of the validations of the required parameters
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="password"></param>
        /// <param name="_error"></param>
        /// <returns></returns>
        private Dictionary<string, string> ValidParameterLogin(string parameter, string password, Dictionary<string, string> _error)
        {
            if (string.IsNullOrEmpty(parameter))
                _error.Add("Auth-ValidParameterLogin-01", "El usuario se encuentra vacío");

            if (string.IsNullOrEmpty(password))
                _error.Add("Auth-ValidParameterLogin-01", "La contraseña se encuentra vacía");

            return _error;
        }
        #endregion
    }
}
