﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Manager.Services.Business.Base
{
    /// <summary>
    /// Base class share methods or properties
    /// </summary>
    public abstract class BaseBusiness
    {
        #region Properies
        public const int Page = 1;
        public const int Qty = 10;
        private const string KeyPassword = "m@n@#3Ru$3r";
        #endregion

        /// <summary>
        /// Method in charge of returning a new dictionary
        /// </summary>
        /// <returns></returns>
        protected virtual Dictionary<string, string> GetErrorDictionaryInstance()
            => new Dictionary<string, string>();

        /// <summary>
        /// Method validates if it can execute an action.
        /// </summary>
        /// <param name="errors"></param>
        /// <returns></returns>
        protected virtual bool CanDoAction(Dictionary<string, string> errors)
            => errors.Count == 0;

        /// <summary>
        /// Method in charge calculate total pages
        /// </summary>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        protected virtual int GetTotalPage(decimal totalRecords)
            => (int)Math.Ceiling(Convert.ToDecimal(totalRecords / Qty));

        /// <summary>
        /// Method to encrypt the password
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        protected virtual string Encrypt(string plainText)
        {
            if (plainText == null)
            {
                return null;
            }
            // Get the bytes of the string
            var bytesToBeEncrypted = Encoding.UTF8.GetBytes(plainText);
            var passwordBytes = Encoding.UTF8.GetBytes(KeyPassword);

            // Hash the password with SHA256
            passwordBytes = SHA512.Create().ComputeHash(passwordBytes);

            var bytesEncrypted = Encrypt(bytesToBeEncrypted, passwordBytes);

            return Convert.ToBase64String(bytesEncrypted);
        }

        /// <summary>
        ///Method to encrypt byts
        /// </summary>
        /// <param name="bytesToBeEncrypted"></param>
        /// <param name="passwordBytes"></param>
        /// <returns></returns>
        private byte[] Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            var saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);

                    AES.KeySize = 256;
                    AES.BlockSize = 128;
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }

                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }
    }
}
