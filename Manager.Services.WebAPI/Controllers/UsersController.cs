﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Manager.Services.Business.UserManagementRules.Interface;
using Manager.Services.Entities.DTO.UserDTO;
using Manager.Services.WebAPI.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Manager.Services.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        #region Properties IoC
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserManagementRules _userManagementRules;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor injecta las instancias o implementar
        /// </summary>
        /// <param name="userManagementRules"></param>
        public UsersController(IUserManagementRules userManagementRules,
                               IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _userManagementRules = userManagementRules;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method responsible for the persistence 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post(UserDTO user)
        {
            var autenticacion = AuthHelper.GetCurrentAuth(HttpContext);
            if (autenticacion == 0)
                return Unauthorized(ToolsHelper.GetUnauthorized());

            return Ok(await _userManagementRules.SaveCurrentUser(user));
        } 

        /// <summary>
        ///  Method responsible for the persistence update
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Put(UserDTO user)
        {
            var autenticacion = AuthHelper.GetCurrentAuth(HttpContext);
            if (autenticacion == 0)
                return Unauthorized(ToolsHelper.GetUnauthorized());

            return Ok(await _userManagementRules.SaveCurrentUser(user));
        }

        /// <summary>
        /// Method responsible for the desactive user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpDelete("{userId}")]
        public async Task<IActionResult> Delete(string userId)
        {
            var autenticacion = AuthHelper.GetCurrentAuth(HttpContext);
            if (autenticacion == 0)
                return Unauthorized(ToolsHelper.GetUnauthorized());

           return Ok(await _userManagementRules.DeleteUser(userId));
        }

        /// <summary>
        /// Method responsible for the get all users 
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get(int? page)
        {
            var autenticacion = AuthHelper.GetCurrentAuth(HttpContext);
            if (autenticacion == 0)
                return Unauthorized(ToolsHelper.GetUnauthorized());

            return Ok(await _userManagementRules.GetAllUser(page));
        }
        #endregion
    }
}
