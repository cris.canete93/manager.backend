﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Manager.Services.Business.UserManagementRules.Interface;
using Manager.Services.Entities.DTO.GeneralDTO;
using Manager.Services.WebAPI.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Manager.Services.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        #region Properties IoC
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IAuthManagementRules _authManagementRules;
        private readonly IConfiguration _configuration;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor injecta las instancias o implementar
        /// </summary>
        /// <param name="preDevicesService">Service de Pre-Registros</param>
        public AuthController(IAuthManagementRules authManagementRules,
                              IHttpContextAccessor httpContextAccessor,
                              IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _authManagementRules = authManagementRules;
            _configuration = configuration;
        }
        #endregion

        #region Events Controller 
        /// <summary>
        /// Method in charge of validating the user and creating the token
        /// </summary>
        /// <param name="credential">Usuario || Password</param>
        [HttpPost, Route("Login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login(CredentialDTO credential)
        {
            try
            {
                var response = await _authManagementRules.AuthValidUserCrendecial(credential.Parameters, credential.Password);
                if (response.IsSuccesFully)
                {
                    response.Data.Token = AuthHelper.GenerateToken(_configuration, response.Data);
                    return Ok(response);
                }
                else if (response.Errors.Any())
                    return StatusCode(400, response.Errors);
                else
                    return Unauthorized(response);
            }
            catch (Exception ex)
            {
                return StatusCode(400, ToolsHelper.GetBadRequest(ex));
            }
        }
        #endregion
    }
}
