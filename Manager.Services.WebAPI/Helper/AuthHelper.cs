﻿using Manager.Services.Entities.DTO.UserDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Manager.Services.WebAPI.Helper
{
    /// <summary>
    /// Helper in charge of the utilities or credentials and tokens
    /// </summary>
    public static class AuthHelper
    {
        /// <summary>
        /// Encumbered method of creating the token in the session.
        /// </summary>
        /// <param name="configuration">Property the configuration</param>
        /// <param name="user">current user</param>
        /// <returns></returns>
        public static string GenerateToken(IConfiguration configuration, UserDTO user)
        {
            var _symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:keyToken"]));
            var _signingCredentials = new SigningCredentials(_symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var _Header = new JwtHeader(_signingCredentials);

            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, user.UserId?.ToString()),
                new Claim("AuthId", user.AuthId?.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString())
            };

            var _Payload = new JwtPayload(
                   issuer: configuration["JWT:Issuer"],
                   audience: configuration["JWT:Audience"],
                   claims: _Claims,
                   notBefore: DateTime.UtcNow,
                   expires: DateTime.UtcNow.AddMinutes(Convert.ToUInt32(configuration["ExpiraSession"]))
               );

            var _Token = new JwtSecurityToken(
                   _Header,
                   _Payload
               );

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }

        /// <summary>
        /// Method in charge of returning the user in the current session.
        /// </summary>
        /// <param name="currentUser">Instancia actual del HTTP</param>
        /// <returns></returns>
        public static int GetCurrentAuth(HttpContext currentUser)
        {
            int autenticationId = 0;
            try
            {
                if (currentUser.User.HasClaim(claim => claim.Type == "AuthId"))
                {
                    var elementId = currentUser.User.Claims.FirstOrDefault(claim => claim.Type == "AuthId").Value;
                    if (!string.IsNullOrEmpty(elementId))
                    {
                        int.TryParse(elementId, out autenticationId);
                        return autenticationId;
                    }
                }
            }
            catch (Exception)
            {
                return autenticationId;
            }

            return autenticationId;
        }
    }
}
