﻿using Manager.Services.Entities.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Manager.Services.WebAPI.Helper
{
    /// <summary>
    /// Helper in charge of the sole responsibility of routines and custom tools
    /// </summary>
    public static class ToolsHelper
    {
        #region Properties
        private static Dictionary<string, string> _errors;
        #endregion

        #region Public Methods
        /// <summary>
        /// Method in charge of responding an unauthorized message
        /// </summary>
        /// <returns></returns>
        public static Result<string> GetUnauthorized()
        {
            _errors = new Dictionary<string, string>
            {
                { "Authorize", "Por favor, de validar que tiene los permisos necesearios o se encuentre a autenticado" }
            };
            return new Result<string>("No-Autorizado",_errors);
        }

        /// <summary>
        /// Method in charge of responding an BadRequest message
        /// </summary>
        /// <returns></returns>
        public static Result<string> GetBadRequest(Exception exception)
        {
            _errors = new Dictionary<string, string>
            {
                { "Error-Authorize", $"Ocurrio un error al procesar la solicitud contacte al administrador de sistemas. Error : { exception.Message }" }
            };
            return new Result<string>("Mala-solitud", _errors);
        }
        #endregion

    }
}
