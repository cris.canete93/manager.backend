﻿using Manager.Services.Business.UserManagementRules.Implementation;
using Manager.Services.Business.UserManagementRules.Interface;
using Manager.Services.DataAccess.Repository.Implementation;
using Manager.Services.DataAccess.Repository.Interface;
using Manager.Services.DataAccess.UnitOfWork.Implementation;
using Manager.Services.DataAccess.UnitOfWork.Interface;
using Microsoft.Extensions.DependencyInjection;

namespace Manager.Services.CrossCutting.IOCRegister
{
    /// <summary>
    /// Class In charge of executing the injection in the layers
    /// </summary>
    public static class IoCRegister
    {
        #region Public Method 
        /// <summary>
        ///  Execute the action to do the dependency injection.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            AddRegistrationBusiness(services);
            AddRegistrationDataAccess(services);

            return services;
        }
        #endregion

        #region Private Method
        /// <summary>
        /// Inject into the Business layer
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddRegistrationBusiness(IServiceCollection services)
        {
            services.AddScoped<IUserManagementRules, UserManagementRules>();
            services.AddScoped<IAuthManagementRules, AuthManagementRules>();
            return services;
        }

        /// <summary>
        /// Inject into the DataAccess layer
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddRegistrationDataAccess(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));

            return services;
        }
        #endregion
    }
}
